﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace BreakAwayConsole
{
    public static class Destinations
    {
        public static void PrintDestinations() {
            using (var ctx = new BreakAwayContext()) {
               var data=ctx.Destinations.ToList();
                foreach (var dst in data)
                    Console.WriteLine(dst.Name);
            }
        }

        public static void PrintAustralianDestinations() {
            using (var ctx = new BreakAwayContext())
            {
                var data = ctx.Destinations.Where(it=>it.Country=="Australia").ToList();
                foreach (var dst in data)
                    Console.WriteLine(dst.Name);
            }
        }
    }
}
